# ROSWSL2

ROS installation with docker (WSL2 backend) and nvidia-cuda container.

## METHOD 1 WSLG (RECOMMENDED) - WINDOWS 11

Install wslg from PowerSheel as Administrator:

    wsl --instal -d Ubuntu

Open Ubuntu from start menu and insert your credentials.

More information about wslg installation here https://docs.microsoft.com/en-us/windows/wsl/tutorials/gui-apps

Install ros following the above documentation:
http://wiki.ros.org/noetic/Installation/Ubuntu

And that is all!

[![Installation video](https://img.youtube.com/vi/0mYaFGC99hU/0.jpg)](https://www.youtube.com/watch?v=0mYaFGC99hU)


Features:
+ Fast GUI renderization
+ Fast installation
+ No docker dependecy

## METHOD 2 WSL2 - WINDOWS 10 | 11

Enable WSL Feature:

    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

Enable VM Feature:

    dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

**Restart PC**

Download WSL2 Updater from https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

Set WSL2 as default

    wsl --set-default-version 2

Look for Ubuntu 20.04 on Microsoft Store , then install it.


More information about wsl2 installation here https://docs.microsoft.com/en-us/windows/wsl/install-manual

Install ros following the above documentation:
http://wiki.ros.org/noetic/Installation/Ubuntu

**Features:**
+ Slow GUI renderization
+ Fast installation
+ No docker dependecy



## METHOD 3 DOCKER DESKTOP - WINDOWS 10 | 11

Enable WSL Feature:

    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

Enable VM Feature:

    dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

**Restart PC**

Download WSL2 Updater from https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

Set WSL2 as default

    wsl --set-default-version 2

Look for Ubuntu 20.04 on Microsoft Store , then install it.


More information about wsl2 installation here https://docs.microsoft.com/en-us/windows/wsl/install-manual

Install docker from https://www.docker.com/products/docker-desktop

Once installed open it and enable WSL2 Integration:

![alt Docker Desktop Settings](img/wsl2_docker_settings.jpg)

Open WSL from Start and install ros with docker hub:

    docker pull osrf/ros:noetic-desktop-full
Or edit the Dockerfile and create your own image:

    docker build .

Download a X Server to display GUI choose between VcXsrv (Recommended) and XMing:
+ VcXsrv from https://sourceforge.net/projects/vcxsrv/
+ XMing  from https://sourceforge.net/projects/xming/

Now open WSL terminal from Start Menu and write the following commands.

    echo "export DISPLAY='$(ip route list default | awk '{print $3}'):0'" >> ~/.bashrc
    echo  "export LIBGL_ALWAYS_INDIRECT=1" >> ~/.bashrc
    source ~/.bashrc

Print the display and check if it is your Machine IP Address

    echo $DISPLAY

![alt IP Address Display](img/Display.png)

![alt IP Address Confirmation](img/IpAddress.png)

Now run a docker container with:

    docker run -it --network=host osrf/ros:noetic-desktop-full bash

If you want to open another terminal , open wsl and write the following to search the ros container:

    docker ps 

You will get a list of all containers, then copy the container id and paste in the following command:
    docker exec -it <container-id> bash

And that is all!

**Features:**
+ GUI velocity could be improved with hardware acceleration
+ It could be a good workspace to develop multiple apps


## EXTRA: NVIDIA CUDA FOR DEEP LEARNING TASKS (ONLY FOR COMPUTE TASKS)

More information here: https://docs.nvidia.com/cuda/wsl-user-guide/index.html#ch05-running-containers
