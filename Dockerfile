FROM osrf/ros:noetic-desktop-full

# Install tools
RUN apt-get update && apt-get upgrade -y && apt-get install -y

# Workspace
WORKDIR /ros_ws